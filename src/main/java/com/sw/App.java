package com.sw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * 启动类.
 * @author DanteFung
 * @version v.0.1
 */
@SpringBootApplication
public class App extends SpringBootServletInitializer{
	
	/**
	 * 参数里VM参数设置为：
	-javaagent:.\lib\springloaded-1.2.4.RELEASE.jar -noverify
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}
	
}
