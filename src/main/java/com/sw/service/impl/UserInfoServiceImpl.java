package com.sw.service.impl;

import com.sw.model.UserInfo;
import com.sw.repository.UserInfoRepository;
import com.sw.service.UserInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


@Service
public class UserInfoServiceImpl implements UserInfoService {
	
	@Resource
	private UserInfoRepository userInfoRepository;
	
	@Override
	public UserInfo findByUsername(String username) {
		System.out.println("UserInfoServiceImpl.findByUsername()");
		return userInfoRepository.findByUsername(username);
	}
	
}
