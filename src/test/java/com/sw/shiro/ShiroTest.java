package com.sw.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.UnauthorizedException;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;
import org.apache.shiro.util.ThreadContext;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

/**
 * Created by DanteFung on 2017/8/16.
 */
public class ShiroTest {


    //////////////////////////////////基于资源(权限)的访问控制:(显示角色)START////////////////////////////////////
    //####################################
    //#    基于资源的访问控制(显示角色)，也可叫做基于权限的访问控制，这种方式的一般规则是
    //#   “资源标识符:操作”,即是资源级别的粒度.
    //#
    //#    优点:
    //#    这种方式的好处就是如果要修改基本都是一个资源级别的修改,不会对其他模块代码产生影响,粒度小.
    //#    但是实现起来可能稍微复杂点，需要维护"用户--角色","角色--权限"（资源:操作）之间的关系.

    /**
     *
     * Shiro同样不进行权限的维护，需要我们通过Realm返回相应的权限信息。
     * 只需要维护“用户--角色”之间的关系即可.
     */
    @Test
    public void testIsPermitted(){
        login("classpath:shiro-permission.ini","zhang","123");
        // 判断拥有权限:user:create
        Assert.assertTrue(subject().isPermitted("user:create"));
        // 判断拥有权限:user:update and user:delete
        Assert.assertTrue(subject().isPermittedAll("user:update","user:delete"));
        // 判断没有权限：user:view
        Assert.assertFalse(subject().isPermitted("user:view"));
    }
    /**
     * shiro提供了isPermitted和isPermittedAll用于判断用户是否拥有某个权限
     * 或所有权限，也没有提供如isPermittedAny用于判断拥有某一个权限的接口.
     * 但是失败的情况下会抛出UnauthorizedException异常.
     */
    @Test(expected = UnauthorizedException.class)
    public void testCheckPermission(){
        login("classpath:shiro-permission.ini","zhang","123");
        // 断言拥有权限:user:create
        subject().checkPermission("user:create");
        // 断言拥有权限:user:delete and user:update
        subject().checkPermissions("user:delete","user:update");
        // 断言拥有权限:user:view 失败抛出异常
        subject().checkPermissions("user:view");
    }
    //////////////////////////////////基于资源的访问控制:(显示角色)END

    //////////////////////////////////基于角色的访问控制:粗粒度(隐式角色)START////////////////////////////////////
    //#################
    //#   缺点:
    //#    如果程序中有很多地方进行了角色判断，突然有一种不需要了，
    //#    那么就需要修改相应代码把所有相关的地方进行删除，这就是粗粒度
    //#    造成的问题.

    /**
     * 授权
     * 如果需要在应用中判断用户是否有相应的角色，就需要在相应的Realm中返回角色信息，也就是说
     * Shiro不负责维护用户-角色信息，需要应用提供.
     *
     * Shiro提供了hasRole/hasRoles用户判断用户是否拥有某个角色/
     *
     * shiro只是提供相应的接口方便验证，后续会介绍如何动态获取用户的角色.
     *
     */
    @Test
    public void testHasRole(){
        login("classpath:shiro-role.ini", "zhang", "123");
        //判断拥有角色：role1
        Assert.assertTrue(subject().hasRole("role1"));
        //判断拥有角色：role1 and role2
        Assert.assertTrue(subject().hasAllRoles(Arrays.asList("role1", "role2")));
        //判断拥有角色：role1 and role2 and !role3
        boolean[] result = subject().hasRoles(Arrays.asList("role1", "role2", "role3"));
        Assert.assertEquals(true, result[0]);
        Assert.assertEquals(true, result[1]);
        Assert.assertEquals(false, result[2]);
    }

    /**
     * Shiro提供了hasRole/hasRoles和hasRole/hasAllRoles不同的地方是它在判断为假的情况下会抛出UnauthorizedException
     * 异常.
     */
    @Test(expected = UnauthorizedException.class)
    public void testCheckRole() {
        login("classpath:shiro-role.ini", "zhang", "123");
        //断言拥有角色：role1
        subject().checkRole("role1");
        //断言拥有角色：role1 and role3 失败抛出异常
        subject().checkRoles("role1", "role3");
    }

    //////////////////////////////////粗粒度(隐式角色)END////////////////////////////////////


    protected void login(String configFile, String username, String password) {
        //1、获取SecurityManager工厂，此处使用Ini配置文件初始化SecurityManager
        Factory<SecurityManager> factory =
                new IniSecurityManagerFactory(configFile);

        //2、得到SecurityManager实例 并绑定给SecurityUtils
        org.apache.shiro.mgt.SecurityManager securityManager = factory.getInstance();
        SecurityUtils.setSecurityManager(securityManager);

        //3、得到Subject及创建用户名/密码身份验证Token（即用户身份/凭证）
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);

        subject.login(token);
    }

    public Subject subject(){
        return SecurityUtils.getSubject();
    }


    @After
    public void tearDown() throws Exception{
        // 退出时请解除绑定Subject到线程，否则对下次测试造成影响
        ThreadContext.unbindSubject();
    }


}
