/**
 * Created by DanteFung on 2017/8/17.
 */
package com.sw.shiro;
/**
 * #################
 * Permission
 * #################
 *
 * 字符串通配符权限
 *
 *    资源标识符:操作:对象实例ID
 *
 *   即对哪个资源的哪个实例可以进行什么操作
 *
 *   ":"表示资源/操作/实例的分割;
 *   ","表示操作的分割;
 *   "*"表示任意资源/操作/实例
 *
 *   1、单个资源单个权限
 *   subject().checkPermissions("system:user:update");
 *   用户拥有资源"system:user"的"update"权限.
 *
 *   2、单个资源多个权限
 *   role41=system:user:update,system:user:delete
 *   然后通过如下代码判断
 *   subject().checkPermission("system:user:update","system:user:delete");
 *   用户拥有资源"system:user"的"update"和"delete"权限.
 *
 *   如上可以简写成ini配置(表示角色42拥有system:user资源的update和delete权限)
 *   role42="system:user:update,delete"
 *   subject().checkPermissions("system:user:update,delete");
 *
 *   3、单个资源全部权限
 *   ini配置
 *   role51="system:user:create,update,delete,view"
 *   然后通过如下代码判断
 *   subject().checkPermissions("system:user:create,delete,update:view");
 *   用户拥有资源"system:user"的"create"、"update"、"delete"、"view"所有权限.
 *   如上可以简写成:
 *   ini配置(表示角色5拥有system:user的所有权限):
 *   role52=system:user:*
 *   也可以简写为(推荐上边的写法)
 *   role53=system:user
 *   然后通过如下代码判断:
 *   subject().checkPermissions("system:user:*");
 *   subject().checkPermissions("system:user");
 *   通过"system:user:*"验证"system:user:create,delete,update:view"可以，但是反过来是不成立的.
 *
 *
 *   4、所有资源全部权限
 *   ini配置
 *   role61=*:view
 *   然后通过如下代码判断:
 *   subject().checkPermissions("user:view");
 *   用户拥有所有资源的"view"所有权限.假设判断的权限是"system:user:view"，那么需要"role5=*:*:view"这样才行.
 *
 *   5、实例级别的权限
 *   5.1、单个实例单个权限
 *   ini配置
 *   role71=user:view:1
 *   对资源user的1实例拥有view权限
 *   然后通过如下代码判断
 *   subject().checkPermissions("user:view:1");
 *
 *   5.2、单个实例多个权限
 *   ini配置
 *   role72=user:update:,delete:1
 *   对资源user的1实例拥有update、delete权限.
 *   然后通过如下代码判断:
 *   subject().checkPermissions("user:delete,update:1");
 *   subject().checkPermissions("user:update:1","user:delete:1");
 *
 *   5.3、单个实例所有权限
 *   ini配置
 *   role73=user:*:1
 *   对资源suer的1实例拥有所有权限
 *   然后通过如下代码判断
 *   subject().checkPermissions("user:update:1","user:delete:1","user:view:1");
 *
 *   5.4、所有实例单个权限
 *   ini配置
 *   role74=user:auth:*
 *   对资源user的所有实例拥有
 *   然后通过如下代码判断
 *   subject().checkPermissions("user:auth:1","user:auth:2");
 *
 *   5.5、所有实例所有权限
 *   ini配置
 *   role75=user:*:*
 *   对资源user的所有实例拥有所有权限
 *   然后通过如下代码判断
 *   subject().checkPermissions("user:view:1","user:auth:2");
 *
 *   前缀匹配可以省略后面的
 *
 *   "*:*:view" 后缀匹配必须指定前缀(多个冒号就需要多个*来匹配)
 *
 *   7、WildcardPermission
 *   如下两种方式是等价的:
 *   subject().checkPermissions("menu:view:1");
 *   subject().checkPermissions(new WildcardPermission("menu:view:1"));
 *   因此，没什么必要的话使用字符串更方便.
 */
