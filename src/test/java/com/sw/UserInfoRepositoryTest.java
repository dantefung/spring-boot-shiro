package com.sw;

import com.sw.model.UserInfo;
import com.sw.repository.UserInfoRepository;
import org.junit.Test;

import javax.annotation.Resource;

/**
 * Created by DanteFung on 2017/8/14.
 */
public class UserInfoRepositoryTest extends Tester{

    @Resource
    private UserInfoRepository repository;

    @Test
    public void testQuery(){
        String username = "admin";
        UserInfo userInfo = repository.findByUsername(username);
        System.out.println(userInfo);
        System.out.println(userInfo.getRoleList());
    }
}
