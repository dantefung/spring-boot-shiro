package com.sw;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by DanteFung on 2017/8/14.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
@org.springframework.transaction.annotation.Transactional
@Rollback
public class Tester {
}
